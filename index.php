
<?php
    session_start();
 
if (isset($_SESSION["statusMsg"])) {
    $statusMsg = $_SESSION["statusMsg"];
}else{
    $statusMsg = "";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Document</title>
</head>
<body>

<div class="status">
<?php echo !empty($statusMsg) ? $statusMsg : "" ; ?>
</div>


<form action="server.php" method="post" enctype="multipart/form-data">
    Select Image File to Upload:
    <input type="file" name="file">
    <input type="submit" name="submit" value="Upload">
</form>

<div id="mydiv">
<?php
$files = glob("uploads/*.*");
for ($i=0; $i<count($files); $i++)
{
	$num = $files[$i];
	echo '<img src="'.$num.'" alt="random image">'."&nbsp;&nbsp;";
}
?>
</div>
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="./script.js"></script>
</html>