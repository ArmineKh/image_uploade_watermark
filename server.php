<?php
class Server{
    // Path configuration 
protected $targetDir = "uploads/"; 
protected $watermarkImagePath = 'logo.png'; 

    function __construct(){
        session_start(); 
        // Processing form data when form is submitted
       if($_SERVER["REQUEST_METHOD"] == "POST"){
           if (isset($_POST['submit'])) {
               $this->uploade();
           }else if(isset($_POST['status'])){
               $this->watermart_all_images();
           }
       }
    }
    function uploade(){
        $statusMsg = ''; 
        if(!empty($_FILES["file"]["name"])){ 
            // File upload path 
            $fileName = basename($_FILES["file"]["name"]); 
            $targetFilePath = $this->targetDir . $fileName; 
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION); 
             
            // Allow certain file formats 
            $allowTypes = array('jpg','png','jpeg'); 
            if(in_array($fileType, $allowTypes)){ 
                // Upload file to the server 
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                    // Load the stamp and the photo to apply the watermark to 
                    $watermarkImg = imagecreatefrompng($this->watermarkImagePath); 
                    switch($fileType){ 
                        case 'jpg': 
                            $im = imagecreatefromjpeg($targetFilePath); 
                            break; 
                        case 'jpeg': 
                            $im = imagecreatefromjpeg($targetFilePath); 
                            break; 
                        case 'png': 
                            $im = imagecreatefrompng($targetFilePath); 
                            break; 
                        default: 
                            $im = imagecreatefromjpeg($targetFilePath); 
                    } 
                     
                    // Set the margins for the watermark 
                    $marge_right = 10; 
                    $marge_bottom = 10; 
                     
                    // Get the height/width of the watermark image 
                    $sx = imagesx($watermarkImg); 
                    $sy = imagesy($watermarkImg); 
                     
                    // Copy the watermark image onto our photo using the margin offsets and  
                    // the photo width to calculate the positioning of the watermark. 
                    imagecopy($im, $watermarkImg, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($watermarkImg), imagesy($watermarkImg)); 
                     
                    // Save image and free memory 
                    imagepng($im, $targetFilePath); 
                    imagedestroy($im); 
         
                    if(file_exists($targetFilePath)){ 
                        $statusMsg = "The image with watermark has been uploaded successfully."; 
                    }else{ 
                        $statusMsg = "Image upload failed, please try again."; 
                    }  
                }else{ 
                    $statusMsg = "Sorry, there was an error uploading your file."; 
                } 
            }else{ 
                $statusMsg = 'Sorry, only JPG, JPEG, and PNG files are allowed to upload.'; 
            } 
        }else{ 
            $statusMsg = 'Please select a file to upload.'; 
        } 


        $_SESSION["statusMsg"] = $statusMsg;
        header("location:index.php");
    }

    function watermart_all_images(){
        $files = glob('uploads/*.{jpg,png,jpeg}', GLOB_BRACE);
        foreach($files as $file) {

            $fileType = pathinfo($file,PATHINFO_EXTENSION);
            switch($fileType){ 
                case 'jpg': 
                    $im = imagecreatefromjpeg($file); 
                    break; 
                case 'jpeg': 
                    $im = imagecreatefromjpeg($file); 
                    break; 
                case 'png': 
                    $im = imagecreatefrompng($file); 
                    break; 
                default: 
                    $im = imagecreatefromjpeg($file); 
            } 

        $watermarkImg = imagecreatefrompng($this->watermarkImagePath);

                $marge_right = 10; 
                $marge_bottom = 10; 

                $sx = imagesx($watermarkImg); 
                $sy = imagesy($watermarkImg); 
                imagecopy($im, $watermarkImg, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($watermarkImg), imagesy($watermarkImg)); 
                imagepng($im, $file); 
                imagedestroy($im);

        
        }

    }
}
$server = new Server();
